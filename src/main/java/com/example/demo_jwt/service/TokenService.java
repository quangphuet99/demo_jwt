
package com.example.demo_jwt.service;

import com.example.demo_jwt.handler.TokenHandler;
import com.example.demo_jwt.models.entity.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    @Autowired
    private TokenHandler tokenHandler;

    public Token createToken(Token token){
        return tokenHandler.createToken(token);
    }

    public Token findByToken(String token){
        return tokenHandler.findByToken(token);
    }
}

