package com.example.demo_jwt.service;

import com.example.demo_jwt.handler.RoleHandler;
import com.example.demo_jwt.handler.UserHandler;
import com.example.demo_jwt.models.entity.Role;
import com.example.demo_jwt.models.entity.User;
import com.example.demo_jwt.models.request.User.UserRegisterRequest;
import com.example.demo_jwt.models.response.User.UserResponse;
import com.example.demo_jwt.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserHandler userHandler;

    @Autowired
    private RoleHandler roleHandler;

    public UserResponse createUser(UserRegisterRequest userRegisterRequest) {
        User user = userRegisterRequest.convertRequestToUser();
        Role role = roleHandler.findById(userRegisterRequest.getRoleId());
        user.setRole(role);
        UserResponse userResponse = new UserResponse(userHandler.saveUser(user));
        return userResponse;
    }

    public UserPrincipal findByUsername(String username) {
        User user = userHandler.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getRole()) {
                Role role = user.getRole();
                authorities.add(role.getRoleKey());
            }

            userPrincipal.setUserId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setAuthorities(authorities);
        }
        return userPrincipal;
    }

}
