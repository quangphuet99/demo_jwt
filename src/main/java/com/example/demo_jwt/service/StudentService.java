package com.example.demo_jwt.service;

import com.example.demo_jwt.handler.StudentHandler;
import com.example.demo_jwt.models.entity.Student;
import com.example.demo_jwt.models.response.Student.StudentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentHandler studentHandler;

    public List<StudentResponse> getAllStudents() {
        List<StudentResponse> responses = new ArrayList<>();
        List<Student> studentList = studentHandler.getAllStudents();
        for (Student student : studentList) {
            StudentResponse response = new StudentResponse(student);
            responses.add(response);
        }
        return  responses;
    }
}
