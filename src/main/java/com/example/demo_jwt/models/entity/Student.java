package com.example.demo_jwt.models.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "t_student")
@Entity
public class Student {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer age;
    private String major;
    private Integer gender;
    private String studentCode;

    public Student (Student student) {
        this.setAge(student.getAge());
        this.setStudentCode(student.getStudentCode());
        this.setName(student.getName());
        this.setGender(student.getGender());
        this.setMajor(student.getMajor());
    }

}
