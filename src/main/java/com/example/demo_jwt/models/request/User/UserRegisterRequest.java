package com.example.demo_jwt.models.request.User;

import com.example.demo_jwt.models.entity.User;
import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Data
public class UserRegisterRequest {
    private Long id;
    private String username;
    private String password;
    private Long roleId;

    public User convertRequestToUser() {
        User user = new User();
        user.setId(this.getId());
        user.setUsername(this.getUsername());
        String password = new BCryptPasswordEncoder().encode(this.getPassword());
        user.setPassword(password);

        return user;
    }

    public User toUser(User user) {
        user.setId(this.getId());
        user.setUsername(this.getUsername());
        String password = new BCryptPasswordEncoder().encode(this.getPassword());
        user.setPassword(password);

        return user;
    }
}
