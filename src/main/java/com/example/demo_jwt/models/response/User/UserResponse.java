package com.example.demo_jwt.models.response.User;

import com.example.demo_jwt.models.entity.Role;
import com.example.demo_jwt.models.entity.User;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserResponse implements Serializable {
    private Long id;
    private String username;
    private Long roleId;

    public UserResponse() {

    }

    public UserResponse(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.roleId = user.getRole().getId();
    }
}
