package com.example.demo_jwt.models.response.Student;

import com.example.demo_jwt.models.entity.Student;
import lombok.Data;

import java.io.Serializable;

@Data
public class StudentResponse implements Serializable {

    private Long id;
    private String name;
    private Integer age;
    private String major;
    private String gender;
    private String studentCode;

    public StudentResponse() {

    }

    public StudentResponse (Student student) {
        this.id = student.getId();
        this.name = student.getName();
        this.age = student.getAge();
        this.major = student.getMajor();
        if (student.getGender() == 1) {
            this.gender = "Nam";
        }
        else {
            this.gender = "Nữ";
        }
        this.studentCode = student.getStudentCode();
    }
}
