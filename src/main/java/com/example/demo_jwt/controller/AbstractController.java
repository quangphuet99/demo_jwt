package com.example.demo_jwt.controller;

import com.example.demo_jwt.common.PagingMeta;
import com.example.demo_jwt.common.Response.ResponseData;
import com.example.demo_jwt.common.Response.ResponseNoData;
import com.example.demo_jwt.common.Response.ResponseListData;



import java.io.Serializable;
import java.util.Collection;

public abstract class AbstractController {

    /**
     * ResponseDataを返す
     *
     * @param data
     * @param <T>
     * @return responseData
     */
    protected <T extends Serializable> ResponseData<T> returnResponseData(T data){
        ResponseData<T> responseData = new ResponseData<>();
        responseData.setCode("0");
        responseData.setMessage("");
        responseData.setData(data);

        return responseData;
    }

    /**
     * ResponseNoDataを返す
     *
     * @return responseNoData
     */
    protected ResponseNoData returnResponseNoData(){
        ResponseNoData responseNoData = new ResponseNoData();
        responseNoData.setCode("0");
        responseNoData.setMessage("");

        return responseNoData;
    }

    /**
     * ResponseListDataを返す
     *
     * @param dataList
     * @param pagingMeta
     * @param <T>
     * @return responseListData
     */
    protected <T extends Serializable> ResponseListData<T> returnResponseListData(Collection<T> dataList, PagingMeta pagingMeta){
        ResponseListData<T> responseListData = new ResponseListData<>();
        responseListData.setCode("0");
        responseListData.setMessage("");
        responseListData.setData(dataList);
        responseListData.setMeta(pagingMeta);

        return responseListData;
    }
}
