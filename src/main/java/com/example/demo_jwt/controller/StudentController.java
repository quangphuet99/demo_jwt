package com.example.demo_jwt.controller;

import com.example.demo_jwt.common.PagingMeta;
import com.example.demo_jwt.common.Response.ResponseListData;
import com.example.demo_jwt.models.response.Student.StudentResponse;
import com.example.demo_jwt.security.UserPrincipal;
import com.example.demo_jwt.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/student")
public class StudentController extends AbstractController {

    @Autowired
    StudentService studentService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/getAllStudents")
    public ResponseEntity<ResponseListData<StudentResponse>> getAllStudents() {
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (principal instanceof UserDetails) {
//            UserPrincipal userPrincipal = (UserPrincipal) principal;
//        }
        PagingMeta meta = new PagingMeta();
        List<StudentResponse> responseListData = studentService.getAllStudents();
        return ResponseEntity.ok(returnResponseListData(responseListData, meta));
    }
}
