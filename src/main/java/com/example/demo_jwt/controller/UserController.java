package com.example.demo_jwt.controller;

import com.example.demo_jwt.common.Response.ResponseData;
import com.example.demo_jwt.models.request.User.UserRegisterRequest;
import com.example.demo_jwt.models.response.User.UserResponse;
import com.example.demo_jwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController extends AbstractController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<ResponseData<UserResponse>> register(@RequestBody UserRegisterRequest userRegisterRequest) {
        UserResponse newUser = userService.createUser(userRegisterRequest);
        return ResponseEntity.ok(returnResponseData(newUser));
    }
}
