package com.example.demo_jwt.controller;

import com.example.demo_jwt.common.Response.ResponseData;
import com.example.demo_jwt.models.entity.Token;
import com.example.demo_jwt.models.entity.User;
import com.example.demo_jwt.security.JwtUtil;
import com.example.demo_jwt.security.UserPrincipal;
import com.example.demo_jwt.service.TokenService;
import com.example.demo_jwt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class AuthController extends AbstractController {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/login")
    public ResponseEntity<ResponseData<String>> login(@RequestBody User user) throws Exception {
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            throw new Exception("tài khoản hoặc mật khẩu không chính xác");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        System.out.println("Token - " + token.getToken());
//        String tokenStr = jwtUtil.generateToken(userPrincipal);
        return ResponseEntity.ok(returnResponseData(token.getToken()));
    }

    @GetMapping("/hello")
    @PreAuthorize("hasAnyAuthority('USER_READ')")
    public ResponseEntity hello() {
        return ResponseEntity.ok("hello");
    }

    @GetMapping("/test")
    public String test() {
        String a = "test";
        return a;
    }
}