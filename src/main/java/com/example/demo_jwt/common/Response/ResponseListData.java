package com.example.demo_jwt.common.Response;


import com.example.demo_jwt.common.PagingMeta;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
public class ResponseListData<T extends Serializable> implements Serializable {

    /**
     * Response Type
     */
    private String type;

    /**
     * Response Message
     */
    private String message;

    /**
     * Response Code
     */
    private String code;

    /**
     * Response data
     */
    private Collection<T> data;

    /**
     * Paging Handle
     * Ignore on result if null
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private PagingMeta meta;

    public void setData(Collection<T> data) {
        this.message = "Success";
        this.code = "200";
        this.data = data;
    }
}
