package com.example.demo_jwt.common.Response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ResponseData<T extends Serializable> implements Serializable {
    private String type;

    private String message;

    private String code;

    private T data;

    public ResponseData(T data) {
        this.data = data;
    }
}
