package com.example.demo_jwt.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Handle paging for a list of records
 */
@Getter
@Setter
@NoArgsConstructor
public class PagingMeta implements Serializable {
    private static final long serialVersionUID = 2405172041950251807L;

    /**
     * Total Record, default 0
     */
    private int total;

    /**
     * Current page number
     */
    private int pageNum;

    /**
     * Number of records on a page
     */
    private int pageSize;

    /**
     * Index of first record on current page
     */
    @JsonIgnore
    private int from;

    /**
     * Index of last record on current page
     */
    @JsonIgnore
    private int to;
//  anh.chuhoang UPDATE 20/01/2021 Start
    /**
     * Sort of type by ascending or descending (ASC/DESC)
     */

    @JsonIgnore
    private String sortType;

    /**
     * Sort by column name
     */
    @JsonIgnore
    private String sortBy;

    /**
     * Create new object with current page number and total records, sort type, sort
     * by on a page
     *
     * @param pageNum  current page number
     * @param sortType sort of type by ascending or descending
     * @param pageSize number of record on a page
     * @param sortBy   sort by column name
     */
    public PagingMeta(int total, int pageNum, int pageSize, String sortBy, String sortType) {
        this.total = total;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.sortType = sortType;
        this.sortBy = sortBy;
    }

    @JsonIgnore
    public boolean isSort() {
        return this.sortType != null && this.sortBy != null && !this.sortType.isEmpty() && !this.sortBy
                .isEmpty();
    }

    /**
     * Create new object with current page number and total records on a page
     * @param pageNum current page number
     * @param pageSize number of record on a page
     */
    public PagingMeta(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;

        this.from = pageSize * (pageNum - 1) + 1;
        this.to = from;
    }

    /**
     * Update last index (to)
     * @param noOfRecords number of records added to list
     */
    public void updateLastRecord(int noOfRecords) {
        this.to = this.from + noOfRecords;
    }

    @JsonIgnore
    public int getOffset() {
        return pageSize * (pageNum - 1);
    }

    @JsonIgnore
    public String getSortType() {
        if (this.sortType.equalsIgnoreCase("desc")) {
            return "desc";
        }
        return "asc";
    }
}