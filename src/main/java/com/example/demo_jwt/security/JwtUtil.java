package com.example.demo_jwt.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component
public class JwtUtil {

    private static Logger logger = LoggerFactory.getLogger(JwtUtil.class);

    private static final String USER = "user";

    @Value("${app.jwtSecret}")
    private String SECRET;

    // tạo mã token
    public String generateToken(UserPrincipal user) {
        String token = null;
        try {

            JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();
            builder.claim(USER, user); // set yêu cầu được chỉ định
            builder.expirationTime(generateExpirationDate());
            JWTClaimsSet claimsSet = builder.build();
            SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
            JWSSigner signer = new MACSigner(SECRET.getBytes());
            signedJWT.sign(signer);
            token = signedJWT.serialize();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return token;
    }

    //tạo ngày hết hạn
    public Date generateExpirationDate() {
        // ~ 1hour
        return new Date(System.currentTimeMillis() + 60000);
    }

    // get thông tin từ token
    private JWTClaimsSet getClaimsFromToken(String token) {
        JWTClaimsSet claims = null;
        try {
            // lấy đối tượng JWS từ token truyền lên
            SignedJWT signedJWT = SignedJWT.parse(token);

            //
            JWSVerifier verifier = new MACVerifier(SECRET.getBytes());
            // Checks the signature of this JWS object with the specified verifier.
            // Kiểm tra chữ ký của đối tượng JWS này với trình xác minh được chỉ định.
            if (signedJWT.verify(verifier)) {
                claims = signedJWT.getJWTClaimsSet();
            }
        } catch (ParseException | JOSEException e) {
            logger.error(e.getMessage());
        }
        return claims;
    }

    // get user từ token
    public UserPrincipal getUserFromToken(String token) {
        UserPrincipal user = null;
        try {
            JWTClaimsSet claims = getClaimsFromToken(token);
            if (claims != null) {
                if (!isTokenExpired(claims)) {
                    JSONObject jsonObject = (JSONObject) claims.getClaim(USER);
                    user = new ObjectMapper().readValue(jsonObject.toJSONString(), UserPrincipal.class);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    // kiểm tra xem token hết hạn hay chưa
    private boolean isTokenExpired(JWTClaimsSet claims) {
        return claims.getExpirationTime().before(new Date());
    }


//    private Date getExpirationDateFromToken(String token) {
//        Date expiration = null;
//        JWTClaimsSet claims = getClaimsFromToken(token);
//        expiration = claims.getExpirationTime();
//        return expiration;
//    }
//
//    private Boolean isTokenExpired(String token) {
//        Date expiration = getExpirationDateFromToken(token);
//        return expiration.before(new Date());
//    }
}