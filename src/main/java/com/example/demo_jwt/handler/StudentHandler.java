package com.example.demo_jwt.handler;

import com.example.demo_jwt.models.entity.Student;
import com.example.demo_jwt.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class StudentHandler {

    @Autowired
    StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        List<Student> studentList = new ArrayList<>();
        studentList = studentRepository.findAll();
        return studentList;
    }

}
