package com.example.demo_jwt.handler;

import com.example.demo_jwt.models.entity.Token;
import com.example.demo_jwt.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TokenHandler {

    @Autowired
    private TokenRepository tokenRepository;

    public Token createToken(Token token) {
        return tokenRepository.saveAndFlush(token);
    }

    public Token findByToken(String token) {
        Token tokenNew = new Token();
        List<Token> tokenList = tokenRepository.findAll();
        for (Token t : tokenList) {
            if (token.equals(t.getToken())) {
                tokenNew = t;
            }
        }
        return tokenNew;
    }
}
