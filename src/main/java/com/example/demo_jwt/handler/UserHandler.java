package com.example.demo_jwt.handler;

import com.example.demo_jwt.models.entity.User;
import com.example.demo_jwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserHandler {

    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    public User findByUsername(String username) {
        User user = new User();
        List<User> list = userRepository.findAll();
        for (User u : list) {
            if (username.equals(u.getUsername())) {
                user = u;
            }
        }
        return user;
    }
}
